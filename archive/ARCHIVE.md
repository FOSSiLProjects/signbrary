# Archived Pages

---

These are pages and code snippets that are likely to be deleted in the foreseeable future. I'm just stuffing them in here in case I screw something up and need to come back to an original, or known good.

Unless I've really screwed something up, nothing in the live code will call upon anything within this directory.

