# Signbrary

## A digital signage and content distribution system

This project aims to be a solution for libraries that want to set up a digital signage system that also allows for content distribution. For example, a library could set up a stand alone digital sign at an event that displays events and notices and enables patrons to connect to the device and download eBooks, music, video, and more.

The software itself is aimed at running as a standalone system on a Raspberry Pi, or as an online system connected to an intranet.

### Slide Display

Supports JPG, PNG, GIF. Slides display for 10 seconds before cycling to the next. 

![Teddy Bear Storytime Slide](https://i.imgur.com/Y6OebZI.png "Teddy Bear Storytime")

Signbrary also supports MP4 for video or animated slides.

### Web Site Display

You can add a website into the slide deck and it will display along with your slides for the same period of time as your slides. This is useful for displaying live content or system wide content across multiple displays.

### Content Display

You can offer various types of content for download. eBooks, MP3s, and more. The content listing will show up at the end of the slide deck.

*Note: This is still under development. It works, mostly, but I'll improve it in the next few days.*

![Teddy Bear Storytime Slide](https://i.imgur.com/Vu2hOf9.png "Teddy Bear Storytime")


---

#### Special Thanks

Signbrary utilizes:

The [Javascript Date Picker](https://github.com/chrishulbert/datepicker) by [Chris Hulbert](http://www.splinter.com.au/).

[The 4 Col Portfolio](https://github.com/BlackrockDigital/startbootstrap-4-col-portfolio) Bootstrap template from BlackrockDigital.

---

**Signbrary is currently under active development. Some features are not yet available.**
