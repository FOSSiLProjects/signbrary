<?php require('login.php');?>
<html>

<head>
  <title>Signbrary - Manage Content</title>
  <link rel="stylesheet" type="text/css" href="../css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="../css/layout.css" />
  <script type="text/javascript" src="../js/datepicker.js"></script>
  <script type="text/javascript" src="../js/timepicker.js"></script>
  <script type="text/javascript" src="../js/w3.js"></script>
  <style>
  #slides {
      font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
      border-collapse: collapse;
      width: 90%;
  }

  #slides td, #slides th {
      border: 1px solid #ddd;
      padding: 8px;
  }

  #slides tr:nth-child(even){background-color: #f2f2f2;}

  #slides tr:hover {background-color: #ddd;}

  #slides th {
      padding-top: 12px;
      padding-bottom: 12px;
      text-align: left;
      background-color: #0000ff;
      color: white;
  }
  </style>
</head>

<body>
  <div id="pagewidth">
    <div id="header"><h2>Signbrary &ndash; Digital Signage. Digital Content.</h2></div>
    <div id="wrapper" class="clearfix">
      <div id="maincol"><h1>Manage Content</h1>

<?php include '../creds.php';?>

<table id="slides">
  <tr>
    <th><strong>Item ID</strong></th>
    <th><strong>Title</strong></th>
    <th><strong>Author</strong></th>
    <th><strong>Format</strong></th>
    <th><strong>Genre</strong></th>
    <th><strong>Brief Summary</strong></th>
    <th><strong>Full Summary</strong></th>
    <th><strong>Cover</strong></th>
    <th><strong>Active</strong></th>
    <th><strong>Downloads</strong></th>
    <th><strong>Edit</strong></th>
    <th><strong>Delete</strong></th>

  </tr>

<?php

$result = mysqli_query($conn,"SELECT * FROM content ORDER BY Title");

while($row = mysqli_fetch_array($result))
{

  $row_contentID = $row['ContentID'];
  $row_id = $row['ItemID'];
  $row_title = $row['Title'];
  $row_author = $row['Author'];
  $row_format = $row['Format'];
  $row_genre = $row['Genre'];
  $row_bsum = $row['SummaryBrief'];
  $row_fsum = $row['SummaryFull'];
  $row_cover = $row['Cover'];
  $row_active = $row['Active'];
  $row_downloads = $row['Downloads'];

  echo '<tr>';
  echo '<td>' . $row_id . '</td>';
  echo '<td>' . $row_title . '</td>';
  echo '<td>' . $row_author . '</td>';
  echo '<td>' . $row_format . '</td>';
  echo '<td>' . $row_genre . '</td>';
  echo '<td>' . $row_bsum . '</td>';
  echo '<td>' . $row_fsum . '</td>';
  echo '<td><img src="../' . $row_cover . '" width="100"</td>';
  echo '<td><a href="content-toggleactive.php?id=' . $row_contentID . '">'.$row_active.'</a></td>';
  echo '<td>' . $row_downloads . '</td>';
  echo '<td><a href="content-edit.php?id=' . $row_contentID . '">Edit</a></td>';
  echo '<td><a href="content-delete.php?id=' . $row_contentID . '" onclick="return confirm(\'Are you sure you want to delete this content?\');">Delete</a></td>';
  echo '</tr>';
}

mysqli_close($conn);
?>

</table>
</div> <!-- End maincol -->

<div id="leftcol">
<p w3-include-html="admin-nav.html"></p>

<script>
w3.includeHTML();
</script>

</div> <!-- End leftcol -->

</div> <!-- End wrapper -->
</div> <!-- End pagewidth -->

</body>
</html>
