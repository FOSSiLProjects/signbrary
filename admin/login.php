<?php
//password needs to be sha1 encrypted - current password = password
$password = '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8';

session_start();
if (!isset($_SESSION['loggedIn'])) {
    $_SESSION['loggedIn'] = false;
}

if (isset($_POST['password'])) {
    if (sha1($_POST['password']) == $password) {
        $_SESSION['loggedIn'] = true;
    } else {
        die ('Incorrect password');
    }
}

if (!$_SESSION['loggedIn']): ?>

<head>
  <title>Signbrary - Log In</title>
  <link rel="stylesheet" type="text/css" href="../css/layout.css" />

</head>

<html><head><title>Signbrary - Admin Login</title></head>
  <body>
    <div id="pagewidth">
      <div id="header"><h1>Signbrary &ndash; Digital Signage. Digital Content.</h1></div>
      <div id="wrapper" class="clearfix">
        <div id="maincol"><h1>Signbrary Administration</h1>
    <p>Signbrary Admin - Please Login</p>
    <form method="post">
      Password: <input type="password" name="password" autofocus> <br />
      <input type="submit" name="submit" value="Login">
    </form>
  </body>
</html>
  <hr>


</div> <!-- End maincol -->

<div id="leftcol">
<p w3-include-html="admin-nav.html"></p>

<script>
w3.includeHTML();
</script>

</div> <!-- End leftcol -->

</div> <!-- End wrapper -->
</div> <!-- End pagewidth -->
</body>
</html>

<?php
exit();
endif;
?>
