<?php require('login.php');?>
<!DOCTYPE html>
<html>

<head>
  <title>Edit Genre</title>
    <link rel="stylesheet" type="text/css" href="../css/layout.css" />
    <script type="text/javascript" src="../js/w3.js"></script>
</head>

<body>
  <div id="pagewidth">
    <div id="header"><h2>Signbrary &ndash; Digital Signage. Digital Content.</h2></div>
    <div id="wrapper" class="clearfix">
      <div id="maincol"><h1>Edit Genre</h1>
        <p style = "color:#ff0000;">All fields are required.</p>
        <hr>

<?php
$editid = htmlspecialchars($_GET["id"]);

include '../creds.php';

$result = mysqli_query($conn,"SELECT * from genre WHERE GenreID = '$editid'");

while($row = mysqli_fetch_array($result))
{
?>

<form action="genre-update.php" method="post" enctype="multipart/form-data">
  Genre Description:<br />
    <input type="text" name="genre" required value="<?php echo(htmlspecialchars($row['Description'])); ?>" /><br /><br />

    <input type="hidden" name="GenreID" value="<?php echo $editid; ?>" />

    <input type="submit" value="Edit Genre" name="submit">
</form>
<?php
}
?>

</div> <!-- End maincol -->

<div id="leftcol">
<p w3-include-html="admin-nav.html"></p>

<script>
w3.includeHTML();
</script>

</div> <!-- End leftcol -->

</div> <!-- End wrapper -->
</div> <!-- End pagewidth -->

</body>
</html>
