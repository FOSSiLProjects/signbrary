<?php require('login.php');?>
<!DOCTYPE html>
<html>

<head>
  <title>Edit Content</title>
    <link rel="stylesheet" type="text/css" href="../css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="../css/layout.css" />
    <script type="text/javascript" src="../js/w3.js"></script>
    <script type="text/javascript" src="../js/datepicker.js"></script>
    <script type="text/javascript" src="../js/timepicker.js"></script>
</head>

<body>
  <div id="pagewidth">
    <div id="header"><h2>Signbrary &ndash; Digital Signage. Digital Content.</h2></div>
    <div id="wrapper" class="clearfix">
      <div id="maincol"><h1>Edit Content</h1>
        <p style = "color:#ff0000;">All fields are required.</p>
        <hr>

<?php
$editid = htmlspecialchars($_GET["id"]);

include '../creds.php';

$result = mysqli_query($conn,"SELECT * from content WHERE ContentID = '$editid'");
$getgenre = mysqli_query($conn,"SELECT Description from genre order by Description");


while($row = mysqli_fetch_array($result))
{
?>

<form action="content-update.php" method="post" enctype="multipart/form-data" id="updatecontent">
  Item ID:<br />
    <input type="text" name="ItemID" value="<?php echo(htmlspecialchars($row['ItemID'])); ?>" required /><br /><br />

  Title:<br />
    <input type="text" name="cont_title" value="<?php echo(htmlspecialchars($row['Title'])); ?>" required /><br /><br />

  Author:<br />
    <input type="text" name="cont_author" value="<?php echo(htmlspecialchars($row['Author'])); ?>" required /><br /><br />

  Format:<br />
  <input type="radio" name="format" value="mobi" checked="checked">Mobi eBook<br />
  <input type="radio" name="format" value="ePub">ePub eBook<br />
  <input type="radio" name="format" value="PDF">PDF file<br />
  <input type="radio" name="format" value="MP3">MP3 audio<br />
  <input type="radio" name="format" value="ePub">MP4 video<br /><br />

  Brief Summary (Max 250 characters)<br />
  <textarea rows="3" cols="80" maxlength="250" name="bSum" form="updatecontent" required><?php echo(htmlspecialchars($row['SummaryBrief'])); ?></textarea><br /><br />

  Full Summary<br />
    <textarea rows="8" cols="80" name="fSum" form="updatecontent" required><?php echo(htmlspecialchars($row['SummaryFull'])); ?></textarea><br /><br />

  Set Content to Active/Inactive<br />
    <input type="radio" name="activecontent" value="active" checked="checked">Active<br />
    <input type="radio" name="activecontent" value="inactive">Inactive<br /><br />

  Cover:<br />
    <?php
    echo '<img src="'.($row['Cover']).'" width="150"><br /><br />';
    ?>

  Genre:<br />
    <?php

    echo "<select name='genre'>";
    while ($row = mysqli_fetch_array($getgenre)) {
        echo "<option value='" . $row['Description'] . "'>" . $row['Description'] . "</option>";
    }
    echo "</select><br /><br />"; ?>

  Select content for upload (Max size = 20 MB):
    <input type="file" name="fileToUpload" id="fileToUpload" required><br /><br />

  Select cover image for upload (Max size = 10 MB):
    <input type="file" name="coverToUpload" id="coverToUpload" required><br /><br />


  <input type="hidden" name="editID" value="<?php echo $editid; ?>" />

  <input type="submit" value="Edit Content" name="submit">
</form>
<?php
}
?>

</div> <!-- End maincol -->

<div id="leftcol">
<p w3-include-html="admin-nav.html"></p>

<script>
w3.includeHTML();
</script>

</div> <!-- End leftcol -->

</div> <!-- End wrapper -->
</div> <!-- End pagewidth -->

</body>
</html>
