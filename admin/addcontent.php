<?php require('login.php');?>
<!DOCTYPE html>
<html>

<head>
  <title>Signbrary - Add Content</title>
  <link rel="stylesheet" type="text/css" href="../css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="../css/layout.css" />
  <script type="text/javascript" src="../js/datepicker.js"></script>
  <script type="text/javascript" src="../js/timepicker.js"></script>
  <script type="text/javascript" src="../js/w3.js"></script>
</head>

<body>
  <div id="pagewidth">
    <div id="header"><h1>Signbrary &ndash; Digital Signage. Digital Content.</h1></div>
    <div id="wrapper" class="clearfix">
      <div id="maincol"><h1>Add Content</h1>
        <p>Accepted formats include: Mobi, ePUB, PDF, MP3, MP4</p>
        <p style = "color:#ff0000;">All fields are required.</p>
        <hr>

<form action="../content-add.php" method="post" enctype="multipart/form-data" id="addcontent">
  Title:<br />
    <input type="text" name="cont_title" placeholder="Ex: Dubliners" required /><br /><br />

  Author:<br />
    <input type="text" name="cont_author" placeholder="Ex: James Joyce" required /><br /><br />

  Format<br />
  <input type="radio" name="format" value="mobi" checked="checked">Mobi eBook<br />
  <input type="radio" name="format" value="ePub">ePub eBook<br />
  <input type="radio" name="format" value="PDF">PDF file<br />
  <input type="radio" name="format" value="MP3">MP3 audio<br />
  <input type="radio" name="format" value="ePub">MP4 video<br /><br />

  Genre<br />
  <?php

  include '../creds.php';

  $result = mysqli_query($conn,"SELECT Description from genre order by Description");


  echo "<select name='genre'>";
  while ($row = mysqli_fetch_array($result)) {
      echo "<option value='" . $row['Description'] . "'>" . $row['Description'] . "</option>";
  }
  echo "</select><br /><br />";

  ?>


  Brief Summary (Max 250 characters)<br />
    <textarea rows="3" cols="80" maxlength="250" name="bsum" form="addcontent" required></textarea><br /><br />

  Full Summary<br />
    <textarea rows="8" cols="80" name="fsum" form="addcontent" required></textarea><br /><br />

  Set content to Active/Inactive<br />
    <input type="radio" name="activecontent" value="active" checked="checked">Active<br />
    <input type="radio" name="activecontent" value="inactive">Inactive<br /><br />


  Select content for upload (Max size = 20 MB):
    <input type="file" name="fileToUpload" id="fileToUpload" required><br /><br />

  Select cover image for upload (Max size = 10 MB):
    <input type="file" name="coverToUpload" id="coverToUpload" required><br /><br />

    <input type="submit" value="Add Content" name="submit">
</form>

</div> <!-- End maincol -->

<div id="leftcol">
<p w3-include-html="admin-nav.html"></p>

<script>
w3.includeHTML();
</script>

</div> <!-- End leftcol -->

</div> <!-- End wrapper -->
</div> <!-- End pagewidth -->
</body>
</html>
