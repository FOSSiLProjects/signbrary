<?php require('login.php');?>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="../css/layout.css" />
  <script type="text/javascript" src="../js/w3.js"></script>
</head>
<body>
  <div id="pagewidth">
    <div id="header"><h1>Signbrary &ndash; Digital Signage. Digital Content.</h1></div>
    <div id="wrapper" class="clearfix">
      <div id="maincol"><h1>Genre Edit Complete</h1>
<?php

include '../creds.php';

$genre_update = $_POST['genre'];
$editid = $_POST["GenreID"];

// Updating the database

mysqli_query($conn,"UPDATE genre SET Description = '$genre_update' WHERE GenreID = '$editid'");

// Close database connection

mysqli_close($conn);

header('Location: genre-manage.php');

?>

</div> <!-- End maincol -->

<div id="leftcol">
<p w3-include-html="admin-nav.html"></p>

<script>
w3.includeHTML();
</script>

</div> <!-- End leftcol -->

</div> <!-- End wrapper -->
</div> <!-- End pagewidth -->
</body>
</html>
