<?php require('login.php');?>
<html>

<head>
  <title>Signbrary - Manage Genres</title>
  <link rel="stylesheet" type="text/css" href="../css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="../css/layout.css" />
  <script type="text/javascript" src="../js/w3.js"></script>
  <style>
  #slides {
      font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
      border-collapse: collapse;
      width: 90%;
  }

  #slides td, #slides th {
      border: 1px solid #ddd;
      padding: 8px;
  }

  #slides tr:nth-child(even){background-color: #f2f2f2;}

  #slides tr:hover {background-color: #ddd;}

  #slides th {
      padding-top: 12px;
      padding-bottom: 12px;
      text-align: left;
      background-color: #0000ff;
      color: white;
  }
  </style>
</head>

<body>
  <div id="pagewidth">
    <div id="header"><h2>Signbrary &ndash; Digital Signage. Digital Content.</h2></div>
    <div id="wrapper" class="clearfix">
      <div id="maincol"><h1>Manage Genres</h1>

<?php include '../creds.php';?>

<table id="slides">
  <tr>
    <th><strong>Genre</strong></th>
    <th><strong>Edit</strong></th>
    <th><strong>Delete</strong></th>
  </tr>

<?php

$result = mysqli_query($conn,"SELECT * FROM genre ORDER BY Description");

while($row = mysqli_fetch_array($result))
{

  $row_genreid = $row['GenreID'];
  $row_genre = $row['Description'];

  echo '<tr>';
  echo '<td>' . $row_genre . '</td>';
  echo '<td><a href="genre-edit.php?id=' . $row_genreid . '">Edit</a></td>';
  echo '<td><a href="genre-delete.php?id=' . $row_genreid . '" onclick="return confirm(\'Are you sure you want to delete this genre?\');">Delete</a></td>';
  echo '</tr>';
}

mysqli_close($conn);
?>

</table>

<hr><br />

<form action="genre-add.php" method="post" enctype="multipart/form-data" id="addgenre">
  Add a New Genre:<br />
    <input type="text" name="genre" placeholder="Ex: Science Fiction" />

    <input type="submit" value="Add Genre" name="submit">
</form>
</div> <!-- End maincol -->

<div id="leftcol">
<p w3-include-html="admin-nav.html"></p>

<script>
w3.includeHTML();
</script>

</div> <!-- End leftcol -->

</div> <!-- End wrapper -->
</div> <!-- End pagewidth -->

</body>
</html>
