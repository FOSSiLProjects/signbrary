<?php require('login.php');?>
<!DOCTYPE html>
<html>

<head>
  <title>Signbrary - Administration</title>
  <link rel="stylesheet" type="text/css" href="../css/layout.css" />
  <script type="text/javascript" src="../js/w3.js"></script>

</head>

<body>
  <div id="pagewidth">
    <div id="header"><h1>Signbrary &ndash; Digital Signage. Digital Content.</h1></div>
    <div id="wrapper" class="clearfix">
      <div id="maincol"><h1>Signbrary Administration</h1>
        <h3>Digital signage. Digital content.</h3>
        <p>Welcome to Signbrary! On the panel below, you can manage your slides and content.</p>

        <h3 style="color:#00688B; font-variant:small-caps;">Slide Management</h3>
        <p>Add to and control the slides in our digital signage deck.</p>
        <ul>
          <li><a href="addslide.php">Add Slide</a> - Add a new slide to the deck.</li>
          <li><a href="slide-manage.php">Manage Slides</a> - Edit, activate, or delete current slides.</li>
        </ul>
          <h3 style="color:#00688B; font-variant:small-caps;">Content Management</h3>
        <p>Add to and control the content you've made available.</p>
        <ul>
          <li><a href="addcontent.php">Add Content</a> - Add new eBooks, music, or video files to the library.</li>
          <li><a href="content-manage.php">Manage Content</a> - Edit, activate, or delete current content.</li>
          <li><a href="genre-manage.php">Manage Genres</a> - Add, edit, or delete genres for your library.</li>
        </ul>
  <hr>


</div> <!-- End maincol -->

<div id="leftcol">
<p w3-include-html="admin-nav.html"></p>

<script>
w3.includeHTML();
</script>

</div> <!-- End leftcol -->

</div> <!-- End wrapper -->
</div> <!-- End pagewidth -->
</body>
</html>
