-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 17, 2019 at 12:33 PM
-- Server version: 10.1.38-MariaDB-0ubuntu0.18.04.1
-- PHP Version: 7.2.15-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `signbrary`
--

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `ContentID` int(11) NOT NULL COMMENT 'Primary key.',
  `ItemID` text NOT NULL COMMENT 'Defines like titles if needed.',
  `Title` text NOT NULL COMMENT 'Item title.',
  `Author` text NOT NULL COMMENT 'Item author.',
  `Format` tinytext NOT NULL COMMENT 'Mobi, ePUB, PDF, etc.',
  `Genre` tinytext NOT NULL COMMENT 'Item genre.',
  `SummaryBrief` text NOT NULL COMMENT 'Quick summary.',
  `SummaryFull` text NOT NULL COMMENT 'Full summary.',
  `Cover` text NOT NULL COMMENT 'Location of the cover image.',
  `DownloadFile` text NOT NULL COMMENT 'Location of file on the server.',
  `Active` tinytext NOT NULL COMMENT 'Is the content active?',
  `Downloads` int(11) NOT NULL COMMENT 'Number of times the file has been downloaded.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`ContentID`, `ItemID`, `Title`, `Author`, `Format`, `Genre`, `SummaryBrief`, `SummaryFull`, `Cover`, `DownloadFile`, `Active`, `Downloads`) VALUES
(13, 'Walden', 'Walden', 'Henry David Thoreau', 'ePub', 'Non Fiction - Biography', 'A reflection upon simple living in natural surrounding.', 'A reflection upon simple living in natural surrounding.', 'content/covers/walden.jpg', 'content/pg205.epub', 'active', 0),
(16, 'Dubliners', 'Dubliners', 'James Joyce', 'mobi', 'Erotica', 'James Joyce\'s classic collection of the stories about the people of Dublin.', 'James Joyce\'s classic collection of the stories about the people of Dublin.', 'content/covers/dubliners-cover.jpg', 'content/dubliners.mobi', 'active', 0),
(17, 'Alice in Wonderland', 'Alice in Wonderland', 'Lewis Carroll', 'mobi', 'Literary Fiction', 'A young girl falls down a rabbit hole and emerges into a surreal world of magical creatures.', 'A young girl falls down a rabbit hole and emerges into a surreal world of magical creatures.', 'content/covers/alicewonderland.jpg', 'content/AliceInWonderland.mobi', 'active', 0),
(21, 'Divine Comedy', 'Divine Comedy', 'Dante', 'mobi', 'Literary Fiction', 'The story of a trip through hell, purgatory, and heaven.', 'The story of a trip through hell, purgatory, and heaven.', 'content/covers/divinecomedy.jpg', 'content/DivineComedy.mobi', 'active', 0),
(23, 'Anne of Green Gables', 'Anne of Green Gables', 'Lucy Maud Montgomery', 'mobi', 'Literary Fiction', 'The life of a young girl growing up on Prince Edward Island, Canada.', 'The life of a young girl growing up on Prince Edward Island, Canada.', 'content/covers/annegreengables.jpg', 'content/AnneOfGreenGables.mobi', 'active', 0),
(24, 'Pride and Prejudice', 'Pride and Prejudice', 'Jane Austen', 'mobi', 'Literary Fiction', 'Elizabeth Bennet learns the error of making hasty judgments and comes to appreciate the difference between the superficial and the essential. ', 'Elizabeth Bennet learns the error of making hasty judgments and comes to appreciate the difference between the superficial and the essential. ', 'content/covers/prideprejudice.jpg', 'content/PrideAndPrejudice.mobi', 'active', 0),
(25, 'Adventures of Sherlock Holmes', 'Adventures of Sherlock Holmes', 'Arthur Conan Doyle', 'mobi', 'Mystery', 'The world\'s greatest detective takes on cases brought to him by a wide variety of characters seeking his help.', 'The world\'s greatest detective takes on cases brought to him by a wide variety of characters seeking his help.', 'content/covers/sherlockholmes.jpg', 'content/AdventuresOfSherlockHolmes.mobi', 'active', 0),
(31, 'Treasure Island', 'Treasure Island', 'Robert Louis Stevenson', 'mobi', 'Literary Fiction', 'A tale of adventure on the high seas with pirates and buried gold.', 'A tale of adventure on the high seas with pirates and buried gold.', 'content/covers/treasureisland.jpg', 'content/TreasureIsland.mobi', 'active', 0),
(32, 'War of the Worlds', 'War of the Worlds', 'H. G. Wells', 'mobi', 'Science Fiction', 'The original story of alien invasion as humankind fights for survival against hostile Martians.', 'The original story of alien invasion as humankind fights for survival against hostile Martians.', 'content/covers/waroftheworlds.jpg', 'content/WarOfTheWorlds.mobi', 'active', 0);

-- --------------------------------------------------------

--
-- Table structure for table `downloads`
--

CREATE TABLE `downloads` (
  `DownloadID` int(11) NOT NULL,
  `ContentID` int(11) NOT NULL,
  `DownloadDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Content download tracking and reporting';

--
-- Dumping data for table `downloads`
--

INSERT INTO `downloads` (`DownloadID`, `ContentID`, `DownloadDate`) VALUES
(1, 0, '2019-02-07 08:46:53'),
(2, 16, '2019-02-07 08:48:27'),
(3, 16, '2019-02-07 08:54:05'),
(4, 16, '2019-02-07 08:55:25'),
(5, 25, '2019-02-07 09:00:19'),
(6, 16, '2019-02-07 09:37:55');

-- --------------------------------------------------------

--
-- Table structure for table `genre`
--

CREATE TABLE `genre` (
  `GenreID` int(11) NOT NULL COMMENT 'Genre ID.',
  `Description` tinytext NOT NULL COMMENT 'Description of the genre.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `genre`
--

INSERT INTO `genre` (`GenreID`, `Description`) VALUES
(1, 'Mystery'),
(2, 'Science Fiction'),
(3, 'Fantasy'),
(4, 'Historical'),
(5, 'Faith'),
(6, 'Horror'),
(7, 'Literary Fiction'),
(8, 'Romance'),
(9, 'Erotica'),
(10, 'Humour'),
(11, 'Thriller'),
(12, 'Non Fiction - Biography'),
(13, 'Non Fiction - Science'),
(15, 'Non Fiction - Travel'),
(16, 'Children');

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `SlideID` int(11) NOT NULL COMMENT 'Primary key.',
  `SlideName` text NOT NULL COMMENT 'Friendly name of the slide.',
  `SlideUp` datetime NOT NULL COMMENT 'When the slide goes up.',
  `SlideDown` datetime NOT NULL COMMENT 'When the slide comes down.',
  `SlideActive` tinytext NOT NULL COMMENT 'Active/Inactive slide.',
  `SlideFile` text NOT NULL COMMENT 'The location of the file on the server.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`SlideID`, `SlideName`, `SlideUp`, `SlideDown`, `SlideActive`, `SlideFile`) VALUES
(27, 'AdultColouring PNG', '2019-02-05 12:00:00', '2020-03-12 12:00:00', 'active', 'uploads/slide-AdultColouring.png'),
(28, 'Teddy Bear Storytime', '2019-02-05 12:00:00', '2020-02-12 12:00:00', 'active', 'uploads/slide-TeddyBearStorytime.png'),
(29, 'Learn to Code', '2019-02-05 12:00:00', '2020-02-28 12:00:00', 'active', 'uploads/slide-Coding.png');

-- --------------------------------------------------------

--
-- Table structure for table `websites`
--

CREATE TABLE `websites` (
  `WebID` int(11) NOT NULL COMMENT 'Primary key.',
  `WebName` text NOT NULL COMMENT 'Name of the web slide.',
  `WebUp` datetime NOT NULL COMMENT 'When the slide goes up.',
  `WebDown` datetime NOT NULL COMMENT 'When the slide comes down.',
  `WebActive` tinytext NOT NULL COMMENT 'Active/Inactive slide.',
  `WebURL` text NOT NULL COMMENT 'The URL for the web slide.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Web/URL based slides.';

--
-- Dumping data for table `websites`
--

INSERT INTO `websites` (`WebID`, `WebName`, `WebUp`, `WebDown`, `WebActive`, `WebURL`) VALUES
(1, 'QC Weather', '2019-04-16 12:00:00', '2020-04-17 12:00:00', 'active', 'https://darksky.net/forecast/33.2329,-111.6344/us12/en'),
(2, 'Archive', '2019-04-16 12:00:00', '2020-04-17 12:00:00', 'active', 'https://archive.org');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`ContentID`);

--
-- Indexes for table `downloads`
--
ALTER TABLE `downloads`
  ADD PRIMARY KEY (`DownloadID`);

--
-- Indexes for table `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`GenreID`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`SlideID`);

--
-- Indexes for table `websites`
--
ALTER TABLE `websites`
  ADD PRIMARY KEY (`WebID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `ContentID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key.', AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `downloads`
--
ALTER TABLE `downloads`
  MODIFY `DownloadID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `genre`
--
ALTER TABLE `genre`
  MODIFY `GenreID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Genre ID.', AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `SlideID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key.', AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `websites`
--
ALTER TABLE `websites`
  MODIFY `WebID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key.', AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
