<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Signbrary - Digital Collection</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/4-col-portfolio.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">Signbrary - Digital Collection</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <!-- <li class="nav-item active">
              <a class="nav-link" href="#">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Services</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Contact</a>
            </li> -->
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading -->
      <h1 class="my-4">Available for Download!<br />
        <small>Get a free eBook!</small>
      </h1>

      <div class="row"> <!-- BEGIN ROW -->
<?php

include '../creds.php';

$result = mysqli_query($conn,"SELECT ContentID,Title,Author,Format,Genre,SummaryBrief,Cover,DownloadFile FROM content WHERE Active = 'active' ORDER BY Title");

while($row = mysqli_fetch_array($result))
{

  $row_contentID = $row['ContentID'];
  $row_title = $row['Title'];
  $row_author = $row['Author'];
  $row_format = $row['Format'];
  $row_genre = $row['Genre'];
  $row_bsum = $row['SummaryBrief'];
  $row_cover = $row['Cover'];
  $row_file = $row['DownloadFile'];

      //echo '<div class="row"> <!-- BEGIN ROW -->';
        echo '<div class="col-lg-3 col-md-4 col-sm-6 portfolio-item"> <!-- BEGIN ITEM CARD -->';
          echo '<div class="card h-100">';
            echo '<a href="dlc.php?down='.$row_contentID.'"><img class="card-img-top" src="../'.$row_cover.'" alt="'.$row_title.'"></a>';
            echo '<div class="card-body">';
            echo '<h4 class="card-title">';
                echo '<a href="dlc.php?down='.$row_contentID.'">'.$row_title.'</a>';
              echo '</h4>';
              echo '<p><strong>'.$row_author.'</strong><br />';
              echo $row_genre.'</p>';
              echo '<p class="card-text">'.$row_bsum.'</p>';
            echo '</div>';
          echo '</div>';
        echo '</div> <!-- END ITEM CARD -->';
}
?>
    </div> <!-- END ROW -->




    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white"> &copy; Creative Commons By-NC-SA by Signbrary 2019</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
