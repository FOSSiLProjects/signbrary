<?php require('admin/login.php');?>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="css/layout.css" />
  <script type="text/javascript" src="js/w3.js"></script>
</head>
<body>
  <div id="pagewidth">
    <div id="header"><h1>Signbrary &ndash; Digital Signage. Digital Content.</h1></div>
    <div id="wrapper" class="clearfix">
      <div id="maincol"><h1>Content Added</h1>
<?php

include 'creds.php';

$target_dir = "content/";
$target_coverdir = "content/covers/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$target_cover = $target_coverdir . basename($_FILES["coverToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
$coverFileType = strtolower(pathinfo($target_cover,PATHINFO_EXTENSION));
$mobiFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
$epubFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
$pdfFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
$mp3FileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
$mp4FileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

$Title = $_POST["cont_title"];
$Author = $_POST["cont_author"];
$Format = $_POST["format"];
$Genre = $_POST["genre"];
$bSum = mysqli_real_escape_string($conn, $_POST['bsum']);
$fSum = mysqli_real_escape_string($conn, $_POST['fsum']);
$Active = $_POST["activecontent"];

echo '<h3>Add content - Information</h3>';
echo '<hr>';
echo '<strong>Title:</strong> '.$Title.'<br />';
echo '<strong>Author:</strong> '.$Author.'<br />';
echo '<strong>Format:</strong> '.$Format.'<br />';
echo '<strong>Genre:</strong> '.$Genre.'<br />';
echo '<strong>Brief Summary:</strong> '.$bSum.'<br /><br />';
echo '<strong>Full Summary:</strong> '.$fSum.'<br /><br />';
echo '<strong>Active/Inactive:</strong> '.$Active.'<br />';
echo '<br /><br />';
echo '<strong>Content File:</strong> '.$target_file.'<br />';
echo '<strong>Content Cover:</strong> '.$target_cover.'<br />';

// Check file for image or MP4
if(isset($_POST["submit"])) {
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 20000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats for the cover
if($coverFileType != "jpg" && $coverFileType != "png" && $coverFileType != "jpeg"
&& $coverFileType != "gif") {
    echo "Sorry, only JPG, JPEG, PNG, GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file) && move_uploaded_file($_FILES["coverToUpload"]["tmp_name"], $target_cover)) {
        echo "<br /><br /><strong>File uploaded:</strong> ". basename( $_FILES["fileToUpload"]["name"]). "<br />";
        echo "<strong>Cover uploaded:</strong>". basename( $_FILES["covertToUpload"]["name"]."<br /><br />");
        echo "<strong>File path:</strong> " . $target_file . "<br />";
        echo "<strong>Cover path: " . $target_cover . "<br />";

    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}




// Updating the database

mysqli_query($conn,"INSERT INTO content (ContentID,ItemID,Title,Author,Format,Genre,SummaryBrief,SummaryFull,Cover,DownloadFile,Active,Downloads) VALUES (NULL,'$Title','$Title','$Author','$Format','$Genre','$bSum','$fSum','$target_cover','$target_file','$Active','0')");

// Close database connection

mysqli_close($conn);

echo '<img src="' .$target_cover . '" width="250"><br /><br />';


?>
</div> <!-- End maincol -->

<div id="leftcol">
<p w3-include-html="side-nav.html"></p>

<script>
w3.includeHTML();
</script>

</div> <!-- End leftcol -->

</div> <!-- End wrapper -->
</div> <!-- End pagewidth -->
</body>
</html>
