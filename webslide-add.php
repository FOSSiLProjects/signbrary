<?php require('admin/login.php');?>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="css/layout.css" />
  <script type="text/javascript" src="js/w3.js"></script>
</head>
<body>
  <div id="pagewidth">
    <div id="header"><h1>Signbrary &ndash; Digital Signage. Digital Content.</h1></div>
    <div id="wrapper" class="clearfix">
      <div id="maincol"><h1>Slide Added</h1>
<?php

include 'creds.php';

$web_title = $_POST["web_title"];
$start_dt = $_POST["start_dt"];
$start_tm = $_POST["start_tm"];
$end_dt = $_POST["end_dt"];
$end_tm = $_POST["end_tm"];
$activeslide = $_POST["activeslide"];
$url = $_POST["url"];


// Converting dates to SQL standards

$date = new DateTime();
$newDisplayDate = $date->createFromFormat('j-M-Y', $start_dt);
$displaydate = $newDisplayDate->format('Y-m-d');
$newExpireDate = $date->createFromFormat('j-M-Y', $end_dt);
$expiredate = $newExpireDate->format('Y-m-d');

$time = new DateTime();
$newDisplayTime = $time->createFromFormat('G:i', $start_tm);
$displaytime = $newDisplayTime->format('H:i:s');
$newExpireTime = $time->createFromFormat('G:i', $end_tm);
$expiretime = $newExpireTime->format('H:i:s');

echo "<strong>Slide display starts:</strong> " . $SlideUp = $displaydate . " " . $displaytime;
echo "<br /><br />";
echo "<strong>Slide expires on:</strong> " . $SlideDown = $expiredate . " " . $expiretime;
echo "<br /><br />";
echo "Slide URL: ".$url;


echo '<hr>';
echo '<a href="admin/addwebslide.php">Add another slide</a> | <a href="admin/webslide-manage.php">Manage webslides</a>';


// Updating the database

mysqli_query($conn,"INSERT INTO websites (WebID,WebName,WebUp,WebDown,WebActive,WebURL) VALUES (NULL,'$web_title','$SlideUp','$SlideDown','$activeslide','$url')");

// Close database connection

mysqli_close($conn);

?>
</div> <!-- End maincol -->

<div id="leftcol">
<p w3-include-html="side-nav.html"></p>

<script>
w3.includeHTML();
</script>

</div> <!-- End leftcol -->

</div> <!-- End wrapper -->
</div> <!-- End pagewidth -->
</body>
</html>
